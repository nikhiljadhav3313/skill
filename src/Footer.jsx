import React from 'react'
import './Footer.css'

const Footer = () => {
  return (
    <>
       <div className="ai">
        <div className="aipower2">
        <p className='para1'> Ai Powered <br/>profile creation</p>  
        <p className='para2'>Easy process to create your profile</p>
        <p className='para3'>It streamlines job searches, improves candidate-employer matches, and contributes to a more efficient and effective job-seeking process.</p>
        <p className='para4'>Upload your CV/Resume.</p>
        <p className='para5' >Let system scan it and make your profile almost ready.</p>
        </div>
        <div className="ai_img">
         <img src="./images/ai.png" alt="" className='ai_img'/>
        </div>
       </div>
       <div className="client">
            <p className='clientpara'>What our clients say about us..</p>
        </div>


<div className="brands">
            <img src="./images/image1.png" alt="" className='img1'/>
            <img src="./images/image2.png" alt="" className='img2'/>
            <img src="./images/image3.png" alt="" className='img3'/>
            <img src="./images/image4.png" alt="" className='img4'/>
            <img src="./images/image5.png" alt="" className='img5'/>
        </div>

        <div className="director">
            <div className="paradirector">
            <p className='directorpara'>“Skilotech has been an invaluable partner in our quest for skilled talent. Their platform simplifies the hiring process, connecting us with highly qualified professionals in our industry. The personalized support and insights provided by Skilotech have elevated our recruitment efforts, making them a go-to resource for our talent needs.”</p>
            <p className='directorpara'>John Doe</p>
            <p className='directorpara'>Director</p>
            </div>        
        </div>
    
    
    </>
  )
}

export default Footer
