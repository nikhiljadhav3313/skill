import React from "react"
import Skilotech from "./Skilotech"
import For_candidate from "./for_candidate"
import Ai_all from "./Ai_all"
import Skilotech_project from "./Skilotech_project"
import Footer from "./Footer"
import FooterSection from "./FooterSection"

function App() {


  return (
    <>
     <Skilotech/>
     <For_candidate/>
     <Footer/>
     <FooterSection/>
     {/* <Ai_all/> */}
     {/* <Skilotech_project/> */}
     
    </>
  )
}

export default App
