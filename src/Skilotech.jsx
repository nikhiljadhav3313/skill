import React from "react";
import "./Skilotech.css";

function Skilotech() {
  return (
    <>
      <div id="main_all">
        {/* <div className="header"> */}
        <div className="space">
          <div className="header_option">
            <img src="./images/logo_skilotech.png" alt="" />
            <div className="header_comp">
              <li id="home">Home</li>
              <li id="other_comp">Candidate</li>
              <li id="other_comp">Employer</li>
            </div>
          </div>
          <div className="button">
            <button id="signin">Sign in</button>
            <button
              id="signin"
              style={{
                borderRadius: "12px",
                fontWeight: "700",
                border: "1px solid var(--primary, #06A9EF)",
              }}
            >
              Sign up
            </button>
          </div>
        </div>
        {/* </div> */}

        <div className="main_all">
          <div className="main_both">
            <div className="main">
              <div className="empower">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="1234"
                  height="197"
                  viewBox="0 0 1234 197"
                  fill="none"
                >
                  <path
                    d="M0 0H1234L1095.78 197H0V0Z"
                    fill="#00A2E8"
                    fill-opacity="0.8"
                  />
                </svg>
                <div className="text">
                  <p1> Empowering</p1>
                  <p2>Job Seekers,</p2>
                </div>
              </div>
              <div className="connection">
                <div className="connection_text">
                  <p3>Connecting</p3>
                  <p4>Employers</p4>
                </div>
              </div>
              <div className="info">
                <p>
                  Discover your dream career with SkiloTech. We're your one-stop
                  destination for job opportunities that match your skills and
                  aspirations. Find, apply, and excel in your next job
                  effortlessly. Start your journey to success today!
                </p>
              </div>
              <div className="join_now">
                <button id="join">Join Now</button>
              </div>
            </div>
          </div>
          <div className="animation_class">
            <div className="lady_comp">
              <svg
                className="yellow_animation"
                xmlns="http://www.w3.org/2000/svg"
                width="596"
                height="521"
                viewBox="0 0 596 521"
                fill="none"
              >
                <path
                  d="M424.927 320.523C366.117 474.646 372.115 560.089 221.689 502.689C71.2629 445.289 -42.01 269.997 16.8005 115.874C75.611 -38.2487 391.357 -12.566 541.783 44.8338C692.21 102.234 483.738 166.4 424.927 320.523Z"
                  fill="#FBD515"
                />
              </svg>
              <svg
                className="blue_animation"
                xmlns="http://www.w3.org/2000/svg"
                width="535"
                height="638"
                viewBox="0 0 535 638"
                fill="none"
              >
                <g filter="url(#filter0_b_85_3)">
                  <path
                    d="M455.003 352.309C455.003 546.53 491.069 638 330.064 638C169.058 638 0.734863 492.723 0.734863 298.502C0.734863 104.281 304.89 0 465.896 0C626.901 0 455.003 158.088 455.003 352.309Z"
                    fill="#00A2E8"
                    fill-opacity="0.7"
                  />
                </g>
                <defs>
                  <filter
                    id="filter0_b_85_3"
                    x="-19.2651"
                    y="-20"
                    width="574"
                    height="678"
                    filterUnits="userSpaceOnUse"
                    color-interpolation-filters="sRGB"
                  >
                    <feFlood flood-opacity="0" result="BackgroundImageFix" />
                    <feGaussianBlur in="BackgroundImageFix" stdDeviation="10" />
                    <feComposite
                      in2="SourceAlpha"
                      operator="in"
                      result="effect1_backgroundBlur_85_3"
                    />
                    <feBlend
                      mode="normal"
                      in="SourceGraphic"
                      in2="effect1_backgroundBlur_85_3"
                      result="shape"
                    />
                  </filter>
                </defs>
              </svg>
              <img
                className="lady"
                style={{ position: "absolute" }}
                src="./images/lady_img.png"
                alt=""
              />
              <img
                className="man"
                style={{ position: "absolute" }}
                src="./images/man_img.png"
                al
              />
            </div>
          </div>
        </div>

        <div className="bottom_bar">
          <div className="bottom_left">
            <div className="search">
              <img src="./images/icon_search.png" alt="" />
            </div>

            <div className="input">
              <input
                type="text"
                id="input"
                placeholder="Job title or keyword"
              />
            </div>

            <div className="line"></div>
          </div>
          <div className="bottom_mid">
            <img src="./images/icon_location.png" id="location" alt="" />
            <input
              type="search"
              placeholder="Colney, United Kingdom"
              id="input"
            />
          </div>
          <div className="search_button">
            <button id="search_button">Search</button>
          </div>
        </div>
      </div>

      <div className="footer">
        <p id="trust">Trusted by...</p>
        <div className="trust_img">
          <img src="./images/light.png" alt="" />
          <img src="./images/logoip.png" alt="" />
          <img src="./images/aven.png" alt="" />
          <img src="./images/earth.png" alt="" />
          <img src="./images//logoip2.png" alt="" />
        </div>
      </div>
      
      <p id="popular_job">Popular Job Categories</p>
      <p id="popular_info">
        Discover exciting career opportunities in popular fields, from
        technology to healthcare, finance to marketing, and more.
      </p>

      <div className="job_cat_card">
        {
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map((item, index) => (
          <div className="card" key={index}>
            <img src="./images/budget.png" alt="" />
            <div>
              <p id="card_budget">Finance</p>
              <p id="card_job">1598 jobs</p>
            </div>
          </div>
        ))
        }
      </div>
    </>
  );
}

export default Skilotech;
