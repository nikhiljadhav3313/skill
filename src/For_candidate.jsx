import React from "react";
import "./For_candidate.css";

function For_candidate() {
  return (
    <>
      <div className="for_candidate">
        <div className="candidate_text">
          <p id="for">For </p>
          <p id="candidate">Candidate</p>
        </div>         

        <div className="wrapper">
        {
          [1,2,3,4].map((item,index) =>(
        <div className="create_account" key={index}>
          <div className="eclips">
            <div className="create_acc_inner">
              <div className="create_img">
                <img src="./images/create_acc.png" alt="" />
                <p id="create_acc_text">Create Your Free Account</p>
              </div>
            </div>
          </div>
        </div>
       
         ))
}

 </div>
      </div>
    </>
  );
}

export default For_candidate;
